#!usr/bin/python
# -----------------------------
# 25 April 2014
# Written by Pradeep
# -----------------------------

# read parameter file and store in a dictionary

class ConfigReader:
	def __init__ (self, fileName):
		self.param = self.setParameters(fileName);
	# -----------------------------
	def setParameters (self, fileName):
		dic = {};
		
		reader = open(fileName, "r");
		
		for r in [line.strip().split("=") for line in reader.readlines()]:
		 	dic.update({r[0]:r[1]})
			
		reader.close()
		return dic
	
	def getAllParameters(self):
		return self.param
	
	def getParameters(self, k):
		return self.param[k]

# ----------testing-------------------------
def main():
	cr = ConfigReader("./conf/goMapper.conf")
	for k,w in cr.getAllParameters().items():
		print k , " ", w
	print cr.getParameters("annotFileName")
	
	
if __name__ == "__main__": main()
