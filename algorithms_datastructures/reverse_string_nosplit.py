# You are given a string. You want to reverse the words inside it. Example “this is a string” –> 
# “string a is this”. Design an algorithm and implement it. You are not allowed to use String.Split(). 
#After you are done with the code, test it. What will you test? What tests will you write?

str = "hello I will try my, best	7"
rstr=[]
wrd = ""
for i in str[::-1]:
	if i not in [" ",",",'\t']:
		wrd = wrd+i
	else:
		rstr.append(wrd[::-1])
		rstr.append(i)
		wrd=""

if __name__ == "__main__":	
	print "".join(rstr)
    
#NOTES
'''
Solution in 2 steps:
1) Reverse whole the string char by char.
2) Reverse again the characters in each word.
You need to write a method Reverse(string str, int startPos, int endPos).
To test this method test normal cases first (middle of the word, beginning of the word, end of the word, 1 character only, all leters in the string). Check bounds (e.g. invalid ranges). Test it with Unicode symbols (consisting of several chars). Perform stress test (e.g. 50 MB string).
Write a method ReverseWords(string s). Test it with usual cases (few words with single space between), with a single word, with an empty string, with words with several separators between. Test it with string containing words with capital letters.
#http://www.nakov.com/tag/topological-sorting/
'''