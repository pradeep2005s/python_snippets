# baseConverter algorithm to convert integer values into binary, octa or hexa numbers


class StackRear:
    def __init__(self):
        self.items = []

    def push(self,item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

    def isEmpty(self):
        return self.items == [] 


def baseConverter(number, base):

	s = StackRear()
	result = ""
	hexbase = {'10' : 'A', '11':'B', '12':'C','13':'D','14':'E','15':'F'}

	while number > 0:
	

		if base < 10: 
			s.push(number % base)
			
		if base == 16:
			reminder =  number % base
			
			if reminder > 9:
				s.push(hexbase[str(reminder)])
			else:
				s.push(reminder)

		number = number // base




	while s.size() > 0:
		result = result + str(s.pop())
		
	return result

if __name__ == "__main__":

	print baseConverter(233,2) # 11101001
	print baseConverter(42,2) #101010
	print baseConverter(233,8)
	print baseConverter(256,16)
	print(baseConverter(25,2))
	print(baseConverter(25,16))
	print(baseConverter(25,8))

# Notes
# for alternate implimentation see http://interactivepython.org/courselib/static/pythonds/BasicDS/ConvertingDecimalNumberstoBinaryNumbers.html