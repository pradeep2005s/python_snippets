def nsteps(n, cache):
 ''' calculate number of steps taken in 1 and 2 steps in and n steps stairs'''
	
	if n == 0:
		return 0
	if n == 1:
		return 1
	if n == 2:
		return 2

    # uses cache - memonization technique
	if n in cache.keys():
		return cache[n]
	else:
		cache[n] = nsteps(n-1,cache) + nsteps(n-2,cache)
		return cache[n]
        
if __name__ == "__name__":
    # Test stairs
    print ("Number of steps taken :"+ str(nsteps(20)))
    