# insertion sort

def insert_sort(l):
''' iterate sort list'''
	for i in range(1, len(l)):
		insert(l, i, l[i])

def insert(l, idx, value):
'''insert value into proper position '''
	i = idx-1
	while i>=0 and l[i]>value:
		l[i+1] = l[i]
		i = i-1
	
	l[i+1] = value	

#test	
if __name__ == "__main__":

	alist = [54,26,93,17,77,31,44,55,20]
	insert_sort(alist)
	print alist

# source http://interactivepython.org/runestone/static/pythonds/SortSearch/TheInsertionSort.html