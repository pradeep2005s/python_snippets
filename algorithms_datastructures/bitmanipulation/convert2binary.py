
def convert_binary(n,b_arr):
    ''' convert decimal number given as n  into binary number '''
    max_pow = 2
    # find max_pow of 2 less than n
    while max_pow <= n:
        max_pow *=max_pow

    max_pow =  max_pow / 2
    while max_pow > 0:
        #print max_pow,n
        if max_pow <= n:
            n = n- max_pow
            max_pow = max_pow / 2
            b_arr.append(1)
        else :
            max_pow = max_pow / 2
            b_arr.append(0)
    
    return b_arr

if __name__ =="__main__":
    #Test case1
    print convert_binary(2,[])
    #Test case2
    print convert_binary(161,[])
    #Test case3
    print convert_binary(1,[])
    #Test case4
    print convert_binary(0,[])
    #Test case5
    print convert_binary(254,[])
    #Test case6
    print convert_binary(255,[])
    #Test case7
    print convert_binary(500,[])