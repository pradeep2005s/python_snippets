# Reverse a string
# Take a string and reverse it. 

# simple 1
def reverse_string_1(s):
    return s[::-1]

# unicode reversal
def reverse_string_2(s):
    
    return    ''.join((s[i] for i in xrange(len(s)-1, -1, -1)))

def main():

# test recursive
   print reverse_string_2("hello world")
   print reverse_string_1(" print in reverse")

if __name__ == "__main__": main()


#[notes and references]
# source http://rosettacode.org/wiki/Reversing_a_string
# source http://stackoverflow.com/questions/931092/reverse-a-string-in-python