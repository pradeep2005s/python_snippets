# Division without / or * 
# page 30 , 1-28. [5] Write a function to perform integer division without using either the / or *
#operators. Find a fast way to do it.
# Skiena the algorithm design manual

def division(dividend, divisor):
	
	if dividend == 0:
		return "division by 0: not allowed"

	
	if dividend < 0 or divisor < 0:
		sign = -1

	if (dividend <0 and divisor < 0) or (dividend >0 and divisor > 0):
		sign = 1

	
	dividend = abs(dividend)
	divisor = abs(divisor)
	

	if divisor > dividend:
		return 0

	reminder = 0
	quotient = 0


	while (dividend > divisor):
		reminder = dividend - divisor
		dividend = reminder
		quotient +=1
		

	if sign == -1:
		return quotient - (quotient+quotient)

	return quotient

if __name__ == "__main__":
	print division(10,-3)
	print division(-10,-3)
	print division(10,3)
	print division(0,10)


# Notes
# http://srikar.wordpress.com/
# https://gist.github.com/knaidu/4334006
# http://blog.panictank.net/the-algorithm-design-manual-chapter-1/