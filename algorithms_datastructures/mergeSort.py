# recursive sort of order O(n log(n)) both worst case and average case complexity
# best case is O(n)
# divide and conquer 
# this implementation as sort function and merge function

def mergeSort(m):
    
    if len(m) <= 1:
        return m
    
    middle = len(m) // 2

    left = m[0:middle]
    right = m[middle:]
        
    left = mergeSort(left)
    right = mergeSort(right)
    return list(merge(left, right))

def merge(left, right):
    result = []
    
    left_idx, right_idx = 0,0

    while len(left) > left_idx and len(right) > right_idx:
        #change the direction of this comparison to chate the direction of the sort
        if left[left_idx] <= right[right_idx]:
            result.append(left[left_idx])
            left_idx += 1

        else:
            result.append(right[right_idx])
            right_idx += 1

    if left:
        result.extend(left[left_idx:])
    if right:
        result.extend(right[right_idx:])
    return result


def main():
    #test
    array = [9, 12, 3, 1, 6, 8, 2, 5, 14, 13, 11, 7, 10, 4, 0]
    print mergeSort(array)
    


if __name__ == "__main__": main()


#[notes and references]
# append v/s extend source: http://stackoverflow.com/questions/252703/python-append-vs-extend
# http://rosettacode.org/wiki/Sorting_algorithms/Merge_sort