# Binary Min Heap Implementation - exploits log nature of the binary tree to represent the heap
# keeps the tree blanced by creating complete binary tree
# heap property requires that the root of the tree be the smallest item in the tree

class BinHeap:
	def __init__(self):
		# heap representation using single list
		self.heapList = [0]
		self.currentSize=0


	def percUp(self,i):
		#maintains heap structure property by comparing the newly added item with its parent
		#percolates a new item as far up in the tree as it needs to go to maintain the heap property
		while(i // 2 >0): # iterate through all the parents
			#If the newly added item is less than its parent, then we can swap the item with its parent. 
			if self.heapList[i] < self.heapList[i//2]: # compare values of child at index i with parent i//2
				self.heapList[i], self.heapList[i//2] = self.heapList[i//2], self.heapList[i]
				
			i = i//2

	
	def insert(self,k):
		
		self.heapList.append(k)
		self.currentSize = self.currentSize + 1
		self.percUp(self.currentSize) #to maintain heap structure property

	def percDown(self, i):
		while(i*2) <= self.currentSize: # if child exist
			mc= self.minChild(i) # return index of largest child
			if self.heapList[i] > self.heapList[mc]: #compare parent and child
				self.heapList[i], self.heapList[mc] = self.heapList[mc], self.heapList[i] #swap

			i = mc

	def minChild(self, i):
		if i*2 +1 > self.currentSize: # if no right child
			return i*2 # return left child
		else:
			if self.heapList[i*2] < self.heapList[i*2+1]: # compare left and rigth child
				return i * 2 #return left child
			else:
				return i*2 +1 #return right child

	def delMin(self):
		# easy to remove as by min heap property the root of the tree be the smallest item in the tree
		# hard part restoring full compliance with the heap structure and heap order properties after the root has been removed
		retval = self.heapList[1]
		
		#restore the root item by taking the last item in the list and moving it to the root position
		# this maintains heap structure property
		self.heapList[1] = self.heapList[self.currentSize]
		self.currentSize = self.currentSize -1
		self.heapList.pop()
		#restore the heap order property by pushing the new root node down the tree to its proper position
		self.percDown(1)
		return retval


	def buildHeap (self,alist):
		#build an entire heap from a list of keys
		# gives count of parent nodes because (n/2)+1 will be leaf
		i = len(alist) // 2 

		self.currentSize = len(alist)
		self.heapList = [0] + alist[:]
		while (i>-1):
			self.percDown(i)
			i=i-1 # got to next node of the current mc
"""Since you are starting with a list of one item, 
the list is sorted and you could use binary search to find the right 
position to insert the next key at a cost of approximately O(logn) operations. 
However, remember that inserting an item in the middle of the list may require O(n) 
operations to shift the rest of the list over to make room for the new key. Therefore, 
to insert n keys into the heap would require a total of O(nlogn) operations. However, 
if we start with an entire list then we can build the whole heap in O(n) operations.
"""


# test case 1
b1 = BinHeap()
alist = [1,9,6,5,2,3]
b1.buildHeap(alist)
print b1.currentSize
print b1.heapList

print b1.delMin()
print b1.currentSize
print b1.heapList

b1.insert(50)
print b1.currentSize
print b1.heapList

# test case 2
bh = BinHeap()
bh.insert(5)
bh.insert(7)
bh.insert(3)
bh.insert(11)

print(bh.delMin())
print(bh.delMin())
print(bh.delMin())
print(bh.delMin())