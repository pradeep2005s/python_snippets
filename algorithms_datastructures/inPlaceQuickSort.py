def quicksort(array, left ,right):
    if right > left:
        print left, right
        pivotIndex = left
        pivotNewIndex = partition(array, left, right, pivotIndex)
        quicksort(array, left, pivotNewIndex - 1)
        quicksort(array, pivotNewIndex + 1, right)
        
def swap(array, index1, index2):
    array[index1], array[index2] = array[index2], array[index1]

def partition(array, left, right, pivotIndex):
    pivotValue = array[pivotIndex]
    swap(array, pivotIndex, right)
    storeIndex = left
    for i in range(left, right):    # range doesn't include right element already
        if array[i] <= pivotValue:  # need to check for equality (not really necessary for the sorting routine)
            swap(array, i, storeIndex)
            storeIndex = storeIndex + 1
            print array, i
    swap(array, storeIndex, right)
    return storeIndex
        


def main():
    #test
    array = [3,7,8,5,2,1,9,5,4]
    quicksort(array,0, len(array) - 1)


if __name__ == '__main__': main()
    
    
#[Notes and References]
#partition(array, 0, len(array) - 1, 3) # 5 is pivot
#print array # expecting all the elements to the left of pivot value(5) will be lesser or equal.
#Source from http://stackoverflow.com/questions/12148093/inplace-quick-sort-implementation