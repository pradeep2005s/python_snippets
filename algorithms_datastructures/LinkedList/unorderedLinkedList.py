# UnorderedList implementation

class Node:

    def __init__(self,initData):
        self.data = initData
        self.next = None

    def getData(self):
        return self.data

    def setData(self,newData):
        self.data = newData

    def getNext(self):
        return self.next

    def setNext(self, newNext):
        self.next = newNext

class UnorderedList:

    def __init__(self):
        self.head = None

    def isEmpty(self):
        return self.head == None

    def add(self,item):
        temp = Node(item) # create
        temp.setNext(self.head) # add new node to the front of the existing via head
        self.head = temp # set the head to the new node which is first node

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count+1
            current = current.getNext()
        return count

    def search(self,item):
        current = self.head
        found = False
        while current != None and not found:
            if current.getData() == item:
                found = True
            else:
                current = current.getNext()
        return found

    def removeItem(self,item):
        current = self.head
        previousNode = None
        found  = False
        # list empty
        if self.isEmpty(): 
            raise RuntimeError ('empty list')
            return

        # one item in the list
        if self.size() == 1 and current.getData() == item:
            self.head = None
            return

        # if list contains n and item is the first
        if self.size() > 1 and current.getData() == item:
            self.remove()
        else: # node is last item in the list with n items
        # node is in the middle of the list with n items
            while not found:
                if current.getData() == item:
                    found = True
                else:
                    previousNode = current
                    current = current.getNext()

            previousNode.setNext(current.getNext())


    def remove(self):
        if self.head != None:
            current = self.head
            self.head = current.getNext()
            current = None
    
    def showall(self):
        current = self.head
        while current != None:
            print current.getData()
            current = current.getNext()


if __name__ == "__main__":
    mylist = UnorderedList()
    mylist.add(31)
    mylist.add(77)
    mylist.add(17)
    mylist.add(93)
    mylist.add(26)
    mylist.add(54)

    print "size:", mylist.size()
    print "list contnets"
    mylist.showall()
    print "finding:",
    print mylist.search(17)
    mylist.removeItem(54)
    print "----"
    mylist.showall()

    mylist2 = UnorderedList()
    mylist2.removeItem(7)
    
# Notes : http://interactivepython.org/runestone/static/pythonds/BasicDS/ImplementinganUnorderedListLinkedLists.html
