# reversal of linked list using explicit stack.
# iterative solution time = O(n) and space = O(1)
# recursive solution time = O(n) and space = O(n) - uses implicit stack


class Stack:
     def __init__(self):
         self.items = []
     def isEmpty(self):
         return self.items == []
     def push(self, item):
         self.items.append(item)
     def pop(self):
         return self.items.pop()
     def peek(self):
         return self.items[-1]
     def size(self):
         return len(self.items)


class Node:

    def __init__(self,initData):
        self.data = initData
        self.next = None

    def getData(self):
        return self.data

    def setData(self,newData):
        self.data = newData

    def getNext(self):
        return self.next

    def setNext(self, newNext):
        self.next = newNext

class UnorderedList:

    def __init__(self):
        self.head = None

    def isEmpty(self):
        return self.head == None

    def add(self,item):
        temp = Node(item) # create
        temp.setNext(self.head) # add new node to the front of the existing via head
        self.head = temp # set the head to the new node which is first node

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count+1
            current = current.getNext()
        return count

    def search(self,item):
        current = self.head
        found = False
        while current != None and not found:
            if current.getData() == item:
                found = True
            else:
                current = current.getNext()
        return found

    def removeItem(self,item):
        current = self.head
        previousNode = None
        found  = False
        # list empty
        if self.isEmpty(): 
            raise RuntimeError ('empty list')
            return

        # one item in the list
        if self.size() == 1 and current.getData() == item:
            self.head = None
            return

        # if list contains n and item is the first
        if self.size() > 1 and current.getData() == item:
            self.remove()
        else: # node is last item in the list with n items
        # node is in the middle of the list with n items
            while not found:
                if current.getData() == item:
                    found = True
                else:
                    previousNode = current
                    current = current.getNext()

            previousNode.setNext(current.getNext())


    def remove(self):
        if self.head != None:
            current = self.head
            self.head = current.getNext()
            current = None
    
    def showall(self):
        current = self.head
        tempList = []
        while current != None:
            tempList.append(current.getData())
            current = current.getNext()
        return tempList

    def reverse(self):
        current = self.head
        s = Stack()
        while current != None:
            s.push(current)
            #print s.peek().getData()
            current = current.getNext()

        current = s.peek()
        self.head = current
        s.pop()
        while s.isEmpty() == False:
           #print self.head.getData()
            
            current.setNext(s.peek())
            s.pop()
            current = current.getNext()
        current.setNext(None) # !!! if not set showall will go into infite loop
        

            



if __name__ == "__main__":
    mylist = UnorderedList()
    mylist.add(31)
    mylist.add(77)
    mylist.add(17)
    mylist.add(93)
    mylist.add(26)
    mylist.add(54)

    print "size:", mylist.size()
    print "list contnets"
    print mylist.showall()
    print "finding:",
    print mylist.search(17)
    print "remove"
    mylist.removeItem(54)
    print mylist.showall()
    print "add"
    mylist.add(54)
    print mylist.showall()
    print "reverse"
    mylist.reverse()
    print mylist.showall()


 Notes video http://www.youtube.com/watch?v=hNP72JdOIgY


