# http://www.greenteapress.com/thinkpython/html/thinkpython014.html
# 13.1  Word frequency analysis
# Exercise 1  
# Write a program that reads a file, breaks each line into words, strips whitespace and punctuation from the words, and converts them to lowercase.
# Hint: The string module provides strings named whitespace, which contains space
# >>> import string
# >>> print string.punctuation
# !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
# Also, you might consider using the string methods strip, replace and translate.

import string

class WordFrequencyAnalysis():
    
    def __init__(self):
        self.word_frequenceis = {}


    def process_file(self,file):
            f = open(file,'r')
            for line in f:
                line = self.rem_punct(line)
                
                for word in line.split():
                    word.strip()
                    
                    if word in self.word_frequenceis.keys():
                        self.word_frequenceis[word] = self.word_frequenceis[word]+1
                    else:
                        self.word_frequenceis[word] = 1
    
    
    def rem_punct(self,line):
        ''' remove at line level is more efficient than the remove_punct at word level'''
        for sc in string.punctuation:
            line = line.replace(sc,'')
            
        return line


    def get_fequencies(self):
        print self.word_frequenceis

if __name__ == "__main__":

    w = WordFrequencyAnalysis()
    w.process_file('a.txt')
    w.get_fequencies()

#a.txt
'''
the#! shall shall
^the the +shall -shall
the&* shall
the (shall)
and,
and
and
and
shallshall
the %shall
and$
and
and,
and
shall|
'''