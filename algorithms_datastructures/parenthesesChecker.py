# write an algorithm that will read a string of parentheses from left to right 
# decide whether the symbols are blanced


class StackRear:
    def __init__(self):
        self.items = []

    def push(self,item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

    def isEmpty(self):
        return self.items == [] 


def parenthesesChecker(str):
	s = StackRear()

	str_len = len(str)

	for ch in str:
		
		str_len -=1
		#print str_len, ch, s.size()

	
		if ch == "(":
			s.push(ch)
		

		if ch ==")" and s.pop() != "(":
			result = "unblanced"
		elif str_len == 0 and s.size() != 0:
			result = "unblanced"
		else:
			result = "blanced"
		
		#print s.size()



	return result


if __name__ == "__main__":

	myString = "(()()()("
	print parenthesesChecker(myString) # unblanced
	print(parenthesesChecker('((()))')) # blanced
	print(parenthesesChecker('((i)(0))')) # blanced
	
	

# Notes
# for alternate implimentation see http://interactivepython.org/courselib/static/pythonds/BasicDS/SimpleBalancedParentheses.html