# Palindrome detection
# check if a sequence of characters (or bytes) is a palindrome or not. 
# must return a boolean value 

# simple non-recursive implementation
def is_palindrome(s):
    return s == s[::-1]

# simple recursive implementation
def is_palindrome_r2(s):
    
    if len(s) > 0:
        r = s[0] == s[-1] and is_palindrome_r2(s[1:-1])
    else: r = True
    
    return r

def main():

# test recursive
    print is_palindrome_r2("abxxBa")
    print is_palindrome_r2("abxaxba")
    
# test non recursive    
    print is_palindrome("abxxBa")
    print is_palindrome("abxaxba")

if __name__ == "__main__": main()


#[notes and references]
# source http://rosettacode.org/wiki/Palindrome_detection