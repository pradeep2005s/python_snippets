# Reverse a string or linked list using Stack implementation
# time complexity is O(n)
# Space complexity is O(n)

class Stack:
    ''' Stack class using list in python'''
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)


def reversed(str):
    ''' reverses the string'''
    s = Stack()

    for c in str:
        s.push(c)

    rev_str = []
    #print s.pop()

    while not s.isEmpty():
        rev_str.append(s.pop())

    return "".join(rev_str)


if __name__ == "__main__":
    str = "hello my name is string"
    print reversed(str)


#Notes: video http://www.youtube.com/watch?v=hNP72JdOIgY