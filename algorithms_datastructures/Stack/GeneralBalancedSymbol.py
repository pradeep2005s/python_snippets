# write an algorithm that will read a string of symbols () or [] or {} from left to right 
# decide whether the symbols are blanced


class StackRear:
    def __init__(self):
        self.items = []

    def push(self,item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

    def isEmpty(self):
        return self.items == [] 


def parenthesesChecker(str):
	s = StackRear()

	str_len = len(str)

	open_set = { '(', '[', '{'}
	closed_set = { ')', ']', '}'}

	for ch in str:
		
		str_len -=1
		#print str_len, ch, s.size()

	
		if ch in  open_set:
			s.push(ch)
		

		if ch in closed_set:
			if (ch ==")" and s.pop() != "(") or (ch =="]" and s.pop() != "[") or (ch =="}" and s.pop() != "{"):
				result = "unblanced"
			elif str_len == 0 and s.size() != 0:
				result = "unblanced"
			else:
				result = "blanced"
		
		#print s.size()



	return result


if __name__ == "__main__":

	myString = "((i){0})"
	print parenthesesChecker(myString)
	print(parenthesesChecker('{([])}'))
	print(parenthesesChecker('(()'))
	print(parenthesesChecker('{{([][])}()}'))
	print(parenthesesChecker('[{()]'))	
	

# Notes
# for alternate implimentation see http://interactivepython.org/courselib/static/pythonds/BasicDS/BalancedSymbols(AGeneralCase).html