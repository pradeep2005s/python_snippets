#!python

def check_rnote(note, magazine):
    
    if note = "" or magazine = "":
        return False
    
    #assume note is < magazine
    c_vector = {}


    # build frequency table for note
    for c in note:
        if c in "abcdefghijzlmnopqrstuvwxyz":

            if c not in c_vector.keys():
                c_vector[c] = 1
            else:
                c_vector[c] = c_vector[c]+1

    if c_vector = {}:
        return False
    
    # loop through each character from the c_vector and check if it satisfies our needed constraint
    for k,v in c_vector.iteritems():
        count_c_in_magazine = 0
                
        for c in magazine:
            
            if c == k and count_c_in_magazine <= v:
                #print c,k,count_c_in_magazine
                count_c_in_magazine += 1
        
        if count_c_in_magazine < v or count_c_in_magazine == 0:
            return False

    return True

if __name__ == "__main__":

    note = "dog dong im"
    magazine1 = "dog is good animal" 
    magazine2 = "no animal here" 


    print check_rnote(note, magazine1) # True
    print check_rnote(note, magazine2) # False

