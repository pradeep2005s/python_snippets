#Problem Statement for SerialNumbers
#http://community.topcoder.com/stat?c=problem_statement&pm=8171
#This problem was used for: Single Round Match 366 Round 1 - Division II, Level One 
#TopCoder College Tour SRM 366 - Division I, Level One

def Sort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            compare(i, i+1, alist)


def compare(idx1,idx2, arr):
      
    if len(str(arr[idx1])) > len(str(arr[idx2])):
        arr[idx1],arr[idx2] = arr[idx2], arr[idx1]
        
    elif sum_of_digits(arr[idx1]) != sum_of_digits(arr[idx2]):
        if sum_of_digits(arr[idx1]) > sum_of_digits(arr[idx2]):
            arr[idx1],arr[idx2] = arr[idx2], arr[idx1]
    else: 
        if arr[idx1] > arr[idx2]:
            arr[idx1],arr[idx2] = arr[idx2], arr[idx1] 


def sum_of_digits(x):
    sum = 0
    for c in str(x):
        if c.isdigit():
            sum = sum + int(c)

    return sum


alist = ["ABCD","145C","A","A910","Z321"] #Returns: {"A", "ABCD", "Z321", "145C", "A910" }
Sort(alist)
print(alist)
alist = ["Z19", "Z20"] #Returns: {"Z20", "Z19" }
Sort(alist)
print(alist)
alist = ["34H2BJS6N","PIM12MD7RCOLWW09","PYF1J14TF","FIPJOTEA5"] # Returns: {"FIPJOTEA5", "PYF1J14TF", "34H2BJS6N", "PIM12MD7RCOLWW09" }
Sort(alist)
print(alist)
alist = ["ABCDE", "BCDEF", "ABCDA", "BAAAA", "ACAAA"] #Returns: {"ABCDA", "ABCDE", "ACAAA", "BAAAA", "BCDEF" }
Sort(alist)
print(alist)

