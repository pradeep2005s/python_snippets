'''
Problem Name:	Series
Used In:	TCCC '01 Semifinals 2
Used As:	Division I Level One
Categories:	Simple Search, Iteration

Problem Statement for Series


Problem Statement
    	
Class Name: Series
Method Name: increasingLength
Paramaters: int[]
Returns: int

Implement a class Series, which contains a method increasingLength.
increasingLength takes an int[] and returns the length of the largest strictly
increasing sequence in the int[].  A strictly increasing sequence is a sequence
of numbers such that every number in the sequence after the first is strictly
greater than the number before it.

Here is the method signature:
public int increasingLength(int[] data);

*The int[] will contain Integers between -10000 and 10000, inclusive.
*The int[] will have at least 1 element and at most 1000 elements.

Note:
*A single element of the int[] is a strictly increasing sequence of size 1.
*Read the int[] in the order in which element 0 is before element 1...

Examples:
*If the int[] is {-4, 5, -2, 0 , 4 , 5 , 9 ,9}, the method should return 5.
*If the int[] is {1,0} the method should return 1.
 
Definition
    	
Class:	Series
Method:	increasingLength
Parameters:	int[]
Returns:	int
Method signature:	int increasingLength(int[] param0)
(be sure your method is public)
    
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2010, TopCoder, Inc. All rights reserved.




This problem was used for: 
       Collegiate Challenge Semifinals - Division I, Level One 
       Collegiate Challenge Semifinals - Division I, Level One
 http://community.topcoder.com/stat?c=problem_statement&pm=64&rd=2006      
'''
# can be solved in a better way using stack
# O(n) time complexity and O(n)

def increasingLength(arr):
	increasing_length = 1
	curr_len = 1

	
	if len(arr) == 1:
		increasing_length=1
		return increasing_length
	
	prev = arr[0]

	for i in range(1,len(arr)):
		if arr[i]> prev:
			curr_len = curr_len+1
			prev = arr[i]
		else:
			prev= arr[i]
			if increasing_length < curr_len:
			    increasing_length = curr_len
			curr_len = 1
			
	if increasing_length < curr_len:
	    increasing_length = curr_len
				
	return increasing_length

if __name__ == "__main__":
	arr = [-4, 5, -2, 0 , 4 , 5 , 9 ,9]
	print increasingLength(arr)

    arr = [1,0]
    print increasingLength(arr)

    arr = [0,1]
    print increasingLength(arr)