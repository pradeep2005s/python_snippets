# Write a function revstring(mystr) that uses a stack to reverse the characters in a string.


class StackRear:
    def __init__(self):
        self.items = []

    def push(self,item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

    def isEmpty(self):
        return self.items == [] 
           
   

def revstring(mystr):

    s=StackRear()
    for x in mystr:
        s.push(x)
    result = []
    while not s.isEmpty():
        result.extend(s.pop())
    return  "".join(result)


if __name__ == "__main__":
    
    # test

    
    str = "hello world"
    print revstring(str)
    
   

# question from 
# http://interactivepython.org/courselib/static/pythonds/BasicDS/ImplementingaStackinPython.html