def siftDown(a,start,end):
    # end represents the limit of how far down the heap to sift
    root = start
    
    while root* 2+1 <= end:     # while root has at least one child
        child = root * 2+1       # root * 2+1 points to the left child
        #if the child has sibling and the child's value is less than its siblings
        if child+1 <= end and a[child] < a[child+1]:
            child = child +1     # then point to the right child instead
        if a[root] < a[child]:   # out of max-heap order
            a[root], a[child] = a[child], a[root]
            root = child         # repeat to continue sifting down the child now
        else: 
            return None


def heapify(a, count):
    #start is assigned the index in a of the last parent node
    start = count -2 / 2
    
    while start >= 0:
    # shft down the node at index start to the proper place
    #such that all nodes below the start index are in heap order
    	siftDown(a, start, count -1)
    	start = start-1 
    	#after sifting down the root all nodes / elements are in heap order
    """ while loop can be implemetned with this onliner -: for start in range(count, -1, -1): siftdown (a, start, count-1)"""
    return a

#test
a = [1,5, 2,4,1,2,2,1, 7,8,-8,8]
count = len(a)


print "Before heapify:", a
heapify(a,count)
print "After heapify:", a


