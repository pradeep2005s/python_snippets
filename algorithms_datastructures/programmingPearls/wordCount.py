# count the number of times each word occur in the document.
# printes the word and its frequency
def word_list(file):

	try:
		s = {}
		f = open(file, "r")
		for line in f:
			words = line.split()
			for word in words:
				if word not in s.keys():
					s[word] = 1
				else:
					s[word] = s[word] + 1
	finally:
		f.close()

	return s
# test


def main ():
	file = "a.text"
	print word_list(file)

if __name__ == "__main": main()

#Notes - problem taken from book programming pearls 2nd edition section 15.1, page 8


