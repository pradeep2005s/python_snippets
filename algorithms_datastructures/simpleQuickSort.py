# simple quicksort algorithm, adapted from Wikipedia
# uses extra array : less, pivotList, greater = three empty arrays
# The runtime of Quicksort ranges from O(n log n) with the best pivots, to O(n2) with the worst pivots, where n is the number of elements in the array.


def quickSort(array):
		
	less=[]
	pivotList=[]
	greater=[]
	
	if len(array) <= 1: # empty list sorts to empty list
		return array
        
	else:
		pivot = array[0] # first item as pivot
		for i in array:
			if i < pivot:
				less.append(i)
			elif i > pivot:
				greater.append(i)
			else:
				pivotList.append(i)
		less = quickSort(less) # recurs on less
		greater = quickSort(greater) # recurs on greater
		return less + pivotList + greater # merge step is trival


def main():

	# test cases
	unsorted_array = [4, 65, 2, -31, 0, 99, 83, 782, 1]
	sorted_array = quickSort(unsorted_array)
	print unsorted_array
	print sorted_array


if __name__ == "__main__": main()

#[Notes and References]
# NOTE: A better quicksort algorithm is which works in place, by swapping elements within the array, to avoid the memory allocation of more arrays.
# Source: http://rosettacode.org/wiki/Sorting_algorithms/Quicksort

# main() will be executed only if this file is run directly with pytyon but not if its 
# imported as a module in another pytyon file.
# Source of this trick http://effbot.org/pyfaq/tutor-what-is-if-name-main-for.htm