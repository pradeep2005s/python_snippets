# Deleting a node from Binary search tree

class Node:
    
    def __init__(self,data=0,left = None ,right = None):
        self.data = data
        self.left = left
        self.right = right


def insert(root, data):
    '''Function to Insert Node in a Binary Search Tree'''
    if root is None:
        root = Node(data,None, None)

    elif data <= root.data:
        root.left = insert(root.left, data)
    else:
        root.right = insert(root.right,data)
    return root



def delete(root, data):
    '''Function to search a delete a value from tree.'''
    if root == None: 
        return
    elif data < root.data:
        root.left = delete(root.left,data)
    elif data > root.data:
        root.right = delete(root.right, data)
    else:
        if root.left == None and root.right == None:
            return
        elif root.left == None:
            
            root = root.right
                        
        elif root.right == None:
            
            root = root.left
            
        else:
            temp = find_min(root.right)
            root.data = temp.data
            root.right = delete(root.right, temp.data)
    return root

def find_min(root):
    '''Function to find minimum in a tree.'''
    
    while root.left != None:
        root = root.left    
    return root
        

def inorder(root):
    '''Function to visit nodes in Inorder'''
    if root is None:
        return

    inorder(root.left)
    print root.data
    inorder(root.right)




if __name__ == "__main__":

    root = Node(5)
    root = insert(root,10)
    root = insert(root,3)
    root = insert(root,4)
    root = insert(root,1)
    root = insert(root,12)
    root = insert(root,9)
    root = insert(root,7)
    root = insert(root,11)
    
    '''
    Code To Test the logic
      Creating an example tree
                5
               / \
              3   10
             / \   \
            1   4   11
    '''
    #Print Nodes in Inorder
    print inorder(root)
    print root.right.data
    #Deleting node with value 5, change this value to test other cases
    root = delete(root,5)
    print inorder(root)
    print root.right.data
    
# Notes:
# watch video http://www.youtube.com/watch?v=gcULXE7ViZw&list=PL2_aWCzGMAwLPEZrZIcNEq9ukGWPfLT4A&index=12
# See C implemenation https://gist.github.com/mycodeschool/9465a188248b624afdbf