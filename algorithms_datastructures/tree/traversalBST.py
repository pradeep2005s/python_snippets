# Binary Tree Traversal - Preorder, Inorder, Postorder

class Node:
    
    def __init__(self,data=0,left = None ,right = None):
        self.data = data
        self.left = left
        self.right = right


def insert(root, data):
    '''Function to Insert Node in a Binary Search Tree'''
    if root is None:
        root = Node(data,None, None)

    elif data <= root.data:
        root.left = insert(root.left, data)
    else:
        root.right = insert(root.right,data)
    return root

     

def inorder(root):
    '''Function to visit nodes in Inorder'''
    if root is None:
        return

    inorder(root.left)
    print root.data
    inorder(root.right)

def preorder(root):
    '''Function to visit nodes in preorder'''
    if root is None:
        return

    print root.data
    preorder(root.left)
    preorder(root.right)


def postorder(root):
    '''Function to visit nodes in postorder'''
    if root is None:
        return

    
    postorder(root.left)
    postorder(root.right)
    print root.data


if __name__ == "__main__":

    root = Node(5)
    root = insert(root,10)
    root = insert(root,3)
    root = insert(root,4)
    root = insert(root,1)
    root = insert(root,11)
    
    '''
    Code To Test the logic
      Creating an example tree
                5
               / \
              3   10
             / \   \
            1   4   11
    '''
    #Print Nodes in Inorder
    print "preorder"
    preorder(root)

    print "inorder"
    inorder(root)

    print "postorder"
    postorder(root)
    
#watch video http://www.youtube.com/playlist?list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P
# C++ code https://gist.github.com/mycodeschool/10016271




