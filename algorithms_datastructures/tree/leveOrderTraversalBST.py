# Binary Tree Traversal - Level Order

class Node:
    
    def __init__(self,data=0,left = None ,right = None):
        self.data = data
        self.left = left
        self.right = right


def insert(root, data):
    '''Function to Insert Node in a Binary Search Tree'''
    if root is None:
        root = Node(data,None, None)

    elif data <= root.data:
        root.left = insert(root.left, data)
    else:
        root.right = insert(root.right,data)
    return root

def levelOrder(root):
    ''' Level order traversal function'''
    if root is None: return

    q = []
    q.append(root)
    # while there is at least one discovered node
    while q != []:
        currentNode = q[0]
        q.remove(q[0])
        print currentNode.data
        if currentNode.left is not None: q.append(currentNode.left)
        if currentNode.right is not None: q.append(currentNode.right)



if __name__ == "__main__":

    root = Node(5)
    root = insert(root,10)
    root = insert(root,3)
    root = insert(root,4)
    root = insert(root,1)
    root = insert(root,11)
    
    '''
    Code To Test the logic
      Creating an example tree
                5
               / \
              3   10
             / \   \
            1   4   11
    '''
    #Print Nodes in levelOrder
    print "levelOrder"
    levelOrder(root)

    
#Notes video: http://www.youtube.com/watch?v=86g8jAQug04
#C++ code https://gist.github.com/mycodeschool/9507131




