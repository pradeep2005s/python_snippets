def buildTree(lst,level=0,node_type="root"):
''' navigates and prints out the tree given as list of lists'''
    
    if lst == []:
        return 

    if len(lst) == 1:
        return lst[0]

    if len(lst) > 1:
        level = level + 1
        print "level",level,node_type,":",lst[0]
        buildTree(lst[1],level,"left")
        buildTree(lst[2],level,"right")
        




if __name__ == '__main__':
    #list = ['a', ['b', [], []], ['c', [], ['d', ['e', [], []], []]]]
    list = ['a', ['b', ['d',[],[]], ['e',[],[]] ], ['c', ['f',[],[]], []] ]

    buildTree(list)

# Notes - list representation of tree