#Write a function to sort a collection of integers using heapsort.

#Heapsort is an in-place sorting algorithm with worst case and average 
# complexity of O(n logn)

def heapSort(a,count):
	#input takes an unordered array a of length count

	heapify(a, count) # first place in max-heap order
	print a, "after heapify"
	end = count -1
	while end > 0 : 
		# swap the root(maximum value) of the heap with the last element of the heap
		a[end], a[0] = a[0], a[end]
		#decrement the size of the heap so that the previous max value will stay in its proper place
		end = end-1 # put the heap back in max-heap order
		siftDown(a, 0, end)
	return a


def siftDown(a,start,end):
    # end represents the limit of how far down the heap to sift
    root = start
    
    while root* 2+1 <= end:     # while root has at least one child
        child = root * 2+1       # root * 2+1 points to the left child
        #if the child has sibling and the child's value is less than its siblings
        if child+1 <= end and a[child] < a[child+1]:
            child = child +1     # then point to the right child instead
        if a[root] < a[child]:   # out of max-heap order
            a[root], a[child] = a[child], a[root]
            root = child         # repeat to continue sifting down the child now
        else: 
            return None


def heapify(a, count):
    #start is assigned the index in a of the last parent node
    start = count -2 / 2
    
    while start >= 0:
    # shft down the node at index start to the proper place
    #such that all nodes below the start index are in heap order
    	siftDown(a, start, count -1)
    	start = start-1 
    	#after sifting down the root all nodes / elements are in heap order


#test
a = [1,5, 2,4,1,2,2,1, 7,8,-8,8]
count = len(a)

print a
print heapSort(a,count)


#[notes and references]
# http://interactivepython.org/courselib/static/pythonds/Trees/BinaryHeapImplementation.html
# https://domenicosolazzo.wordpress.com/2010/09/26/heapsort-a-python-example/



