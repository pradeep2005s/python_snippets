# Implementing the Map Abstract Data Type
# put(key,val) Add a new key-value pair to the map. If the key is already in the map then replace the old value with the new value.
# get(key) Given a key, return the value stored in the map or None otherwise.
# handles collision detection by linear probing


class HashTable:
	
	def __init__(self):
		self.size = 11
		self.slots = [None] * self.size
		self.data = [None] * self.size

	def hashfunction(self, key, tablesize):	return key % tablesize
	def rehash(self, oldhash, tablesize): return (oldhash +1) % tablesize

	def put(self, key, value):
		hashvalue = self.hashfunction(key,len(self.slots))


		if self.slots[hashvalue] is None:
			self.slots[hashvalue] = key
			self.data[hashvalue] = value
		else:
			if self.slots[hashvalue] == key:
				self.data[hashvalue] = data # replace
			else:
				nextslot = self.rehash(hashvalue, len(self.slots))
				while self.slots[nextslot] != None and self.slots[nextslot] != key:
					nextslot = self.rehash(nextslot,len(self.slots))

				if self.slots[nextslot] == None:
					self.slots[nextslot] = key
					self.data[nextslot] = value
				else:
					self.data[nextslot] = value # replace


	def get(self, key):

		startslot = self.hashfunction(key,len(self.slots))

		if self.slots[startslot] == key:
			return self.data[startslot]
		else:
			if self.slots[startslot] == None:
				return -1

			nextslot = self.rehash(startslot, len(self.slots))

			if self.slots[nextslot] == key:
				return self.data[nextslot]
			else:
				while (self.slots[nextslot] != None and nextslot != startslot):
					
					nextslot = self.rehash(nextslot, len(self.slots))
					
					if nextslot == startslot:
						print "back to startslot"
						return -1

					if self.slots[nextslot] == key:
						return self.data[nextslot]

			return -1
			
	def __getitem__(self, key):
		return self.get(key)

	def __setitem__(self, key, value):
		self.put(key,value)

			



#test
	
if __name__ == "__main__": 
	h = HashTable()

	h.put(54,"cat")
	h.put(26,"dog")
	h.put(93,"lion")
	H[17]="tiger"
	H[77]="bird"
	H[31]="cow"
	H[44]="goat"
	H[55]="pig"
	H[20]="chicken" # collision

	print h.slots
	print h.data
	print h.get(54) # direct fetch
	print h.get(99) # not present
	print h[17]
	print h[20]# collision



# for alternative implementation 
# http://interactivepython.org/runestone/static/pythonds/SortSearch/Hashing.html