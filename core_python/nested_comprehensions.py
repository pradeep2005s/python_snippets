#!python

# nested comprehensions

def searchval(mydict, term):

	return list(key for key, value in mydict.items() for val in value if term in val)[0]

def search(mydict, searchFor):
    for k in mydict:
        for v in mydict[k]:
        	print k, v
        	if searchFor in v:
        		return k
    return None


if __name__ == "__main__":


# Test 1
myDict = {'age': ['12'], 'address': ['34 Main Street, 212 First Avenue'],
      'firstName': ['Alan', 'Mary-Ann'], 'lastName': ['Stone', 'Lee']}

print searchval(myDict, "212")
#print search(myDict, "212")

# test 2
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print [[el - 1 for el in row] for row in matrix]
print [ e1-1  for row in matrix for e1 in row if e1 >3]




# Notes
'''
# The list comprehension said:
  [ expression
    for line in open('arecibo.txt')
    for char in line.strip() ]

# It therefore meant:
for line in open('arecibo.txt'):
    for char in line.strip():
        list.append(expression)
        
nested_numbers = [[1, 2, 3], [4, 5], [6, 7, 8]]
some_numbers = [number for inner in nested if len(inner) > 2 for number in inner]

results = [i * j for i in range(1, 4) for j in range(1, 5)]
from itertools import product
results = [i * j for i, j in product(range(1, 4), range(1, 5))]

results = []
nucleobases = "GTCA"
for i in nucleobases:
    for j in nucleobases:
        for k in nucleobases:
            for l in nucleobases:
                results.append("".join((i, j, k, l)))
                
nucleobases = "GTCA"
results = ["".join(c) for c in product(nucleobases, repeat=4)]
# The other combinatorial functions in itertools, permutations(), combinations() 
and combinations_with_replacement() are also worth checking out


#http://rhodesmill.org/brandon/2009/nested-comprehensions/
# http://tech.pro/tutorial/1554/four-tricks-for-comprehensions-in-python


'''
