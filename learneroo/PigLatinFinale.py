# Challenge
# Convert all the space-separated words in a given String into pig latin and print the pig latin string on its own line.
# https://www.learneroo.com/modules/23/nodes/173#
#To finish your program, you need to handle sentences, not just words. Create new code that converts each word in a sentence into pig latin and then prints the complete pig latin sentence. For example, when given
#"knowledge is power" your code should print: "owledgeknay isay owerpay"

def do_stuff(text):
	temp = ''
	text = text.split()
	for word in text:
	    vowel_pos = get_vowel_pos(word)
	    vowel_pos = vowel_pos-1
	    #print vowel_pos
	    if vowel_pos == 0:word = word+'ay '
	    else: word = word[vowel_pos:]+word[:vowel_pos]+'ay '
	    
	    temp = temp+word
	
	return temp	      
		
		
                    
def get_vowel_pos(word):
    vowel_pos = 0
    for c in word:
        if c in "aeiou":
            vowel_pos = vowel_pos+1
            break
        else:
            vowel_pos = vowel_pos+1
    return vowel_pos
                

		
if __name__ == "__main__":
    text = "laptop"
    text1 = "knowledge is power"
    #print get_vowel_pos(text)
    print do_stuff(text) #aptoplay
    print do_stuff(text1) # owledgeknay isay owerpa
    
